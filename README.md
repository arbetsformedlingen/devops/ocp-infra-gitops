# GitOps Configuration

* _base_ - a base set of kustomize manifests and yaml for applications, operators, configuration and ArgoCD app/project definitions. Everything is inherited from here
* _overlays_ - environment specific aggregation here.

## argocd app hierarchy
Argocd in each cluster is already watching its cluster specific directory here: 
https://gitlab.com/arbetsformedlingen/devops/argocd-infra/-/tree/main/kustomize/clusters 

To direct argocd to the cluster specific folders here we have added an app there for the test clusters such as: https://gitlab.com/arbetsformedlingen/devops/argocd-infra/-/blob/main/kustomize/clusters/onprem-test/ocp-infra-gitops.yaml 

The same should be done for all clusters

Then in this repo in `overlays/{cluster}/argocd-apps` there is another argocd application for each component to be deployed to the cluster 

This gives you the flexibility to tune the argocd apps per cluster and per operator/infra component in a fine grained manner so you can for example enable pruning for a specific app and disable for an other or enable in one cluster and disable in another etc

So basically to onboard new apps just add a new argocd app manifest along with the base and overlays for the app


## Deploying components from helm charts
Depending on whether a component is being installed sourced from a Helm chart, OLM or Raw manifest will determine somewhat the layout for example when sourcing a helm chart it doesn't make much sense to have anything in base/ cause Chart.yaml and values.yaml cant be kustomized so as seen in overlays/onprem-test/vault we just have Chart.yaml and values.yaml directly and plug in the desired chart as a dependency, argocd will then pull the dependent chart, run a helm template and oc apply

This pattern of adding a Chart.yaml and values.yaml can be used with any 3rd party upstream helm chart that you want to deploy via the gitops

Due to the way things are currently done for argocd the namespaces need to be pre provisioned via the ns-conf-infra project else argocd misses permissions to create the resources in teh namespaces

https://gitlab.com/arbetsformedlingen/devops/ns-conf-infra/-/tree/main/kustomize/overlays

## Deploying components from OLM
When deploying an operator to the clusters from OLM you can add the required manifests here and have argocd do the operator deployment

Here a subscription and operatorgroup + any other desired manifests can be added to base/ and deployed to the clusters using the kustomize overlays  

We recommend having a separate base and overlay for each operator as well as putting each operator in its own namespace which will require creating new operator group per operator instead of the default which is to typically install operators into the openshift-operators namespace

Due to the way things are currently done for argocd the namespaces need to be pre provisioned via the ns-conf-infra project else argocd misses permissions to create the resources in the namespaces

https://gitlab.com/arbetsformedlingen/devops/ns-conf-infra/-/tree/main/kustomize/overlays

## TODO

**vault**
The vault deployment works well, vault comes up and appears to be healthy, is auto-unsealed and initialized and accessible via its route and the vault root token which is found in the `vault-root-token` secrets in the `vault` namespace until LDAP is configured via the ldap auth method:

https://developer.hashicorp.com/vault/docs/auth/ldap

The only "internal" vault configuration that is  done at this point is what is done during the init as can be seen from the terraform files here:
https://gitlab.com/arbetsformedlingen/devops/vault/-/blob/main/images/init/vault-audit.tf
https://gitlab.com/arbetsformedlingen/devops/vault/-/blob/main/images/init/vault-auth-methods.tf 
https://gitlab.com/arbetsformedlingen/devops/vault/-/blob/main/images/init/vault-policies.tf 

Vault can be further configured if these files are changed and the chart redeployed then by deleting the vault-init kubernetes job from the cluster/argocd and letting it resync it which will reapply the terraform code

See the vault terraform provider for details on how to configure vault via terraform: https://registry.terraform.io/providers/hashicorp/vault/latest/docs

The configuration for the vault-config-operator and external-secrets-operator *should* be valid but unfortunately as both operators are having issues further troubleshooting and experimentation will need to be done to work out why they dont work and it is possible it is something with vault or the cluster itself

**external-secrets-operator**

External-secrets-operator needs to be deployed to all clusters, the deployment itself should be fine as is

Currently what is not working as expected on onprem-test is that eso doesnt appear to be able to successfully connect either to vault or the ocp API and needs to be troubleshooted further

The intention is to use vaults kubernetes auth method or alternatively vault JWT/OIDC auth method to enable eso to connect to vault without needing static credentials
https://external-secrets.io/v0.5.1/provider-hashicorp-vault/#kubernetes-authentication 
https://external-secrets.io/v0.5.1/provider-hashicorp-vault/#jwtoidc-authentication 
https://developer.hashicorp.com/vault/docs/auth/jwt/oidc-providers/kubernetes


If for whatever reason the kubernetes authentication continues to be problematic it maybe worth using one of the other authentication types as a workaround:

https://external-secrets.io/v0.5.1/provider-hashicorp-vault/#authentication 

During the POC when eso was running in AWS and Vault on prem we used the JWT/OIDC auth mechanism because this doesnt need vault to be able to reach the remote clusters API for token validation as seen here: 

https://external-secrets.io/v0.5.1/provider-hashicorp-vault/#jwtoidc-authentication 
https://developer.hashicorp.com/vault/docs/auth/jwt/oidc-providers/kubernetes

It maybe worth while to test this also for the onprem-test cluster as an alternative to the kubernetes auth

Basically as the kubernetes auth is not working as expected there is something either in the vault config or cluster specific (potentially proxy, invalid ocp api certificates etc etc) but as we ran out of time i dont have any more solid leads

I would suggest to start again here and get familiar with eso and vault by following through the examples using a token for auth then moving onto testing the various auth methods: https://external-secrets.io/v0.5.1/provider-hashicorp-vault/#example 

To start adding configuration for eso to the gitops such as secretsstore and clustersecretsstore you can add it to base/external-secrets-config or overlay/{cluster}/external-secrets-config

**vault-config-operator**

The intention of the vault-config-operator is to have a kubernetes native way of performing all day 2 configurations of vaults internals, if it continues to be problematic then you may decide to instead continue building out the terraform in the vault init job or even configure vault by hand

vault-config-operator deployment itself is fine, also it is able to authenticate to the vault cluster BUT it is failing to be authorize getting a plain `permission denied` on all operations

The configuration is identical to known good configuration in other clusters so the assumption is the issue is between vault and openshift where vault is failing to do something on the tokenreview api

As we ran out of time to continue troubleshooting this we dont have any solid leads but 

Alot of examples for configuring vault using vco can be found here: https://github.com/redhat-cop/vault-config-operator/tree/main/test 

The important ones for your uses are:

Configuring jwtoidc auth:  
https://github.com/redhat-cop/vault-config-operator/tree/main/test/jwtoidcauthengine

Configuring ldap auth:  
https://github.com/redhat-cop/vault-config-operator/tree/main/test/ldapauthengine

Examples for creating access policies and roles:  
https://github.com/redhat-cop/vault-config-operator/blob/main/test/secret-writer-policy.yaml 
https://github.com/redhat-cop/vault-config-operator/blob/main/test/secret-writer-role.yaml

To start adding configuration for vco such as those referenced above you can add it to base/vault-config or overlay/{cluster}/vault-config

# Reference notes from the hand crafted POC

Below is the reference shell code that was used to configure Vault on onprem-test and ESO on AWS during the POC that was fully functional at the time but has since been cleaned up out of the clusters:

```
from bastion:
kubectl get --raw "$(kubectl get --raw /.well-known/openid-configuration | jq -r '.jwks_uri' | sed -r 's/.*\.[^/]+(.*)/\1/')" > JWK

convert JWK to PEM on https://8gwifi.org/
(JWK has got multiple keys, seperate them)

From vault cli:
vault auth enable jwt
vault write auth/jwt/config \
jwt_validation_pubkeys="-----BEGIN PUBLIC KEY-----
MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA89TiPP8XvKM5HoxfQowS
YTK07S97+JFfO7Fp2Cxre1SfA/hLjsyfusxTpCLaTg5FoMAa6+KfVLXQBgwLZOuM
iUC2S6bkL4iUBfOGjZnQLqKc0opIBLMDzFQrGcaFdRFX/t41BkS4p2bvyvy8qVEN
gv6e+9N4sgZf9P3pveDhDegZdkgFDMmGnGbAFWm1d0lyQ33JQMpRBhceHtqigAr8
C5i1asMJyWJVLgEHr18CEtUUmrfVusDWQk23ubdElgtzUmjCagYSR0T/0hwJEYiF
LT7+Hl/Yw/o/2Qtn0mEz3txkFlxQhx15mBACzxboyuUxAkEOZvpWoNNY5yw0x5wB
oQIDAQAB
-----END PUBLIC KEY-----,
-----BEGIN PUBLIC KEY-----
MIICIjANBgkqhkiG9w0BAQEFAAOCAg8AMIICCgKCAgEAt9zd4M73wHV9XXlztC5O
3LQsTQOynVfiT0EZLGicioBxPV7otY4gwZUN2qnF69Mug6ncc1OLGRliACWlyb0b
DQOzK5sAli5rNPTzZXEFUyYv2pwvdolhNpW03GTP6+b0NavAKq3JOqSjViEZFwfE
glu96zFFOoLRVoVL4QGuwFz8B3L8Ac43H26sDADnekGZqFyxjA+SDJ0ogoAyfi5o
rhvtmfkyeDjcWgzKsOZTOUCaOQim46tlgh705QVKC1F1A7cfAIJMuKV8/YWZzDWW
Gl23uJx3GIQvHw/ZQQ1nBzx6QbDdGLROcbsXNWhhuKSJdQy/RyKNHBxB0bskKn8p
fjzrV9fBoDrqSKDDzXxv+eg1pD+EYrgdSaLML5HzoMmMxe0EAwDSSSI5lvH4Ixnz
9Bwcq0GtHfKhwRwqJXzT6J5uVRZW7Vf+S/CjxCgtcHMjaHA9H2H91/FO+Ty10QJD
Md03NVMiggC0spfQFDZoyS7RhVEh9ox0WETyOE1RTxXdC0GB0qIZ/GWbdrvtemkW
Ab9z4HpUUt04W+dW3EEjnwQniJOqMpvdVGVyGtt5RnH6irIkIbWVBFQwhzNzjYzh
ERdqLd2gjXBRSYXouX2GsOA9SEziUaG29haGNha+dPZknVxOA8I6bD9FHyd0uEjj
/7NKR9gcGWW0NIcs40MySlsCAwEAAQ==
-----END PUBLIC KEY-----,
-----BEGIN PUBLIC KEY-----
MIICIjANBgkqhkiG9w0BAQEFAAOCAg8AMIICCgKCAgEAt9oP8Ttw08W9XRacArqe
dW4FKggvSV7CxIcl4tp2PeWoVFEBWNlCSuJURDaXf/5ldP0yN1s1mqkf7Zqu1npr
ynzQ6BL88guMq0+w6ucapqoNrDd2vyoVyLFGqMAk+xCXiyJf5nDQC0cIcPYwl2XE
I9xH5D8PYJxOKCsfZNdQAMtm/+sMimr1woLJoFgvXzKEuWJDgPwXdUV5vGAdYRVC
nWmgIALtDVMCls1+iuMZRC8jsJ8X6+VjxcI6tLFi+gyKnaeOILWrWOpCQKuR1aUB
6POdjccIf54Qy28ZDFvS6EUMQw9H7GNNSRKslqybAAonXur2xM4cyCNi314xPzG5
qqBlfTZNBCBDi61d/Cyb5/hfDypw+S9TACO33WuibrrSHWlqGQdLfjomH37zc7st
5MzH1J1/UEY3AX2VdIN19igP42ZfTCXKtluOiRmM1Lg7zh40IWJRWzJxMFNr8AY+
LEJXioIdlZ37gwVzltFr5B7g08CJdDqjQG8jkpcW9wvFkzT+x2GznOJkfqm3MthK
biv+Blag6H7IoCdIThuDTUQonA7hkKf382SA3ynOjXHz69DRunYMDLvzZjEVCmmg
f45RypbmE7H4SwUPMwBDI5srgeP0gck4K0Iq9BwR7TEbNPg+q4KJ1WyVfATsowdZ
KsmeK5Vjt4K2iVDEOhEYdBECAwEAAQ==
-----END PUBLIC KEY-----"

vault write auth/jwt/role/external-secrets \
role_type="jwt" \
bound_audiences="https://kubernetes.default.svc" \
user_claim="sub" \
bound_subject="system:serviceaccount:external-secrets-poc:cluster-external-secrets" \
policies="foo" \
ttl="1h" \
verbose_oidc_logging=true

secret-store link to vault Yaml:
apiVersion: external-secrets.io/v1beta1
kind: SecretStore
metadata:
  name: vault-poc
  namespace: external-secrets-poc
spec:
  provider:
    vault:
      auth:
        jwt:
          kubernetesServiceAccountToken:
            audiences:
              - 'https://kubernetes.default.svc'
            expirationSeconds: 600
            serviceAccountRef:
              name: cluster-external-secrets
          path: jwt
          role: external-secrets
      path: secret
      server: 'https://vault-poc.jobtechdev.se'
      version: v2

Add vault policy (we did to through web ui):
Vault policy
path "secret/data/foo" {
  capabilities = ["read"]
}

get a token from a running pod on remote cluster (with the same service account as in bound_subject above):
APISERVER=https://kubernetes.default.svc
SERVICEACCOUNT=/var/run/secrets/kubernetes.io/serviceaccount
NAMESPACE=$(cat ${SERVICEACCOUNT}/namespace) TOKEN=$(cat ${SERVICEACCOUNT}/token) echo $TOKEN
  
use a token to auth:
curl --request POST --data '{"jwt": "<TOKEN>", "role": "external-secrets"}' https://vault-poc.jobtechdev.se/v1/auth/jwt/login

use the returned client_token to fetch a secret:
curl \
--header "X-Vault-Token: <client_token>" \ --url https://vault-poc.jobtechdev.se/v1/secret/data/foo

Example of an external secret to fetch:
apiVersion: external-secrets.io/v1beta1
kind: ExternalSecret
metadata:
  name: vault-example
  namespace: external-secrets-poc
spec:
  data:
    - remoteRef:
        conversionStrategy: Default
        decodingStrategy: None
        key: secret/foo
        property: foo
      secretKey: foo
  refreshInterval: 15s
  secretStoreRef:
    kind: SecretStore
    name: vault-poc
  target:
    creationPolicy: Owner
    deletionPolicy: Retain
    name: vault-example
```
